module gitlab.com/stone.code/scov

go 1.18

require git.sr.ht/~rj/sgr v1.16.2

require (
	golang.org/x/sys v0.26.0 // indirect
	golang.org/x/term v0.25.0 // indirect
)
