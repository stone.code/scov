{ buildGoModule ? (import <nixpkgs> {}).buildGoModule,
  lib ? (import <nixpkgs> {}).lib }:

buildGoModule rec {
  pname = "scov";
  version = "v0.11.0";

  src = lib.cleanSource ./.;

  # No need to build subpackage behemoth, which is only for testing.
  subPackages = [ "." ];

  vendorHash = "sha256-cNlhyIDWTBhE5AnVI9/CPia+eXVgBzpOCClQwnV7+8E=";

  # Update the version information in the built executable
  ldflags = [ "-X" "main.versionInformation=${version}" "-s" "-w" ];

  meta = with lib; {
    description = "Generate reports on code coverage using gcov, lcov, or llvm-cov.";
    homepage = "https://gitlab.com/stone.code/scov";
    license = licenses.bsd3;
    platforms = platforms.all;
  };
}
